---
title: "Multiple FP"
author: "Hans-Michael Kaltenbach"
date: "`r Sys.time()`"
output:
  pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE)
library(dplyr)
library(tidyr)
library(readr)
library(forcats)
library(stringr)
library(knitr)
library(kableExtra)
# Import data
base <- readRDS(file.path(here::here(), "data", "SEROBL_BASE_DATA.RDS"))
poc <- readRDS(file.path(here::here(), "data", "SEROBL_LTA.RDS"))
# Only the various blood donors
neg_all = poc %>% 
  dplyr::filter(collection_before_2020) %>%
  dplyr::mutate(pcr="negative") %>%
  dplyr::select(pid,poc_igm, poc_igg, poc_test_name, pcr)
```

Tables are based on _all_ samples with negative PCR and blood donors. Shown are the IgG/IgM results for each test for those samples with more than one positive IgX result, respectively. A dash indicates that the corresponding measurement is not available.

```{r}
neg_count_igg = neg_all %>%
  dplyr::group_by(pid, poc_igg) %>%
  dplyr::tally() %>%
  dplyr::ungroup() %>%
  dplyr::filter(poc_igg=="positive" & n>1) 

neg_count_igg = inner_join(neg_count_igg, neg_all, by="pid") %>%
  dplyr::mutate(poc_igg.y=replace_na(poc_igg.y, "")) %>%
  dplyr::mutate(poc_igg=if_else(poc_igg.y=="missing", "---", poc_igg.y)) %>%
  dplyr::select(pid, poc_test_name, poc_igg) %>%
  tidyr::pivot_wider(id_cols=c(pid, poc_test_name), names_from=pid, values_from=poc_igg, values_fill=list(poc_igg="---")) %>%
  dplyr::mutate_all(~cell_spec(., bold=if_else(.=="positive", TRUE, FALSE))) %>%
  dplyr::arrange(poc_test_name)

n_igg = ncol(neg_count_igg) - 1
  
kable(neg_count_igg, col.names = c("POCT", 1:n_igg), booktabs=TRUE, linesep="", escape=FALSE) %>%
  kable_styling(latex_options = "scale_down") %>%
  add_header_above(c("", "Patient / IgG result"=n_igg))
```

```{r}
neg_count_igm = neg_all %>%
  group_by(pid, poc_igm) %>%
  tally() %>%
  ungroup() %>%
  filter(poc_igm=="positive" & n>1) 

neg_count_igm = inner_join(neg_count_igm, neg_all, by="pid") %>%
  mutate(poc_igm.y=replace_na(poc_igm.y, "")) %>%
  mutate(poc_igm=if_else(poc_igm.y=="missing", "---", poc_igm.y)) %>%
  dplyr::select(pid, poc_test_name, poc_igm) %>%
  pivot_wider(id_cols=c(pid, poc_test_name), names_from=pid, values_from=poc_igm, values_fill=list(poc_igm="---")) %>%
  mutate_all(~cell_spec(., bold=if_else(.=="positive", TRUE, FALSE))) %>%
  dplyr::arrange(poc_test_name)

n_igm = ncol(neg_count_igm) - 1
  
kable(neg_count_igm, col.names = c("POCT", 1:n_igm), booktabs=TRUE, linesep="", escape="F") %>%
  kable_styling(latex_options = "scale_down") %>%
  add_header_above(c("", "Patient / IgM result"=n_igm))
```

